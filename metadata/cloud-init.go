// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package metadata

import (
	"net/http"

	log "github.com/sirupsen/logrus"
	"gopkg.in/yaml.v2"

	"git.callpipe.com/entanglement.garden/rhyzome-openwrt/config"
)

var (
	userdata = make(map[string][]byte)
)

func cloudInitUserData(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	mac := ctx.Value(requesterMacKey).(string)
	data, ok := userdata[mac]
	if !ok {
		log.Warn("request for non-existant user-data for mac ", mac)
		w.Write([]byte{})
		return
	}
	w.Write(data)
}

// SetUserData stores the user data for a particular instance
func SetUserData(mac string, data []byte) {
	log.Debug("stored user data for ", mac)
	userdata[mac] = data
}

func cloudInitMetaData(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/yaml")
	type cloudInitMetaDataResponse struct {
	}
	err := yaml.NewEncoder(w).Encode(map[string]string{
		"local-hostname": "apiserver",
		"instance-id":    "apiserver",
		"cloud-name":     "rhyzome.dev",
	})
	if err != nil {
		log.Error("Error encoding response", err)
		http.Error(w, err.Error(), 500)
	}
}

func cloudInitVendorData(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte{})
}

func init() {
	config.OnLoad(func() error {
		bootstrapMAC := config.GetBootstrapMAC()
		bootstrapUserData := config.GetBootstrapUserData()

		if bootstrapMAC == "" || len(bootstrapUserData) == 0 {
			log.Debug("bootstrap mac: ", bootstrapMAC)
			log.Debug("bootstrap user data: ", bootstrapUserData)
			return nil
		}

		SetUserData(bootstrapMAC, bootstrapUserData)

		return nil
	})
}
