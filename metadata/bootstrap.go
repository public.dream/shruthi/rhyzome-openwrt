package metadata

import (
	"net/http"
	"strings"

	"git.callpipe.com/entanglement.garden/rhyzome-openwrt/bootstrap"
)

func caAvailable(w http.ResponseWriter, r *http.Request) {
	fingerprint := r.PostFormValue("fingerprint")
	token := r.PostFormValue("token")
	remoteIP := strings.SplitN(r.RemoteAddr, ":", 2)[0]

	go bootstrap.Begin(fingerprint, token, remoteIP)
	w.Write([]byte{})
}
