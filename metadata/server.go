// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package metadata

import (
	"context"
	"net"
	"net/http"
	"strings"

	log "github.com/sirupsen/logrus"

	"github.com/gorilla/mux"
	"github.com/mostlygeek/arp"
)

type contextKey int

const (
	requesterMacKey contextKey = iota
)

var (
	server *http.Server
)

// ListenAndServe starts the metadata server
func ListenAndServe() {
	r := mux.NewRouter()
	r.Use(lookupRequester)
	r.HandleFunc("/cloud-init/user-data", cloudInitUserData)
	r.HandleFunc("/cloud-init/meta-data", cloudInitMetaData)
	r.HandleFunc("/cloud-init/vendor-data", cloudInitVendorData)
	r.HandleFunc("/ca-available", caAvailable)

	server = &http.Server{
		Addr:    "192.168.1.1:8080",
		Handler: LoggingMiddleware(r),
	}
	log.Println("Starting metadata server on", server.Addr)
	err := server.ListenAndServe()
	if err != http.ErrServerClosed {
		log.Fatal("error running metadata server: ", err)
	}
}

func lookupRequester(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ip, _, err := net.SplitHostPort(r.RemoteAddr)
		if err != nil {
			http.Error(w, err.Error(), 404)
			return
		}

		mac := strings.Trim(arp.Search(ip), "'")

		ctx := context.WithValue(r.Context(), requesterMacKey, mac)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

// Shutdown gracefully shuts down the metadata server
func Shutdown() error {
	log.Println("Shutting down metadata server")
	return server.Shutdown(context.Background())
}

type wrapper struct {
	http.ResponseWriter
	written int
	status  int
}

func (w *wrapper) WriteHeader(code int) {
	w.status = code
	w.ResponseWriter.WriteHeader(code)
}

func (w *wrapper) Write(b []byte) (int, error) {
	n, err := w.ResponseWriter.Write(b)
	w.written += n
	return n, err
}

// LoggingMiddleware is a middleware function that logs requests as they come in
func LoggingMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		res := &wrapper{w, 0, 0}
		next.ServeHTTP(res, r)
		log.Debug(r.RemoteAddr, " ", r.Method, " ", r.RequestURI, " ", res.status, " ", res.written)
	})
}
