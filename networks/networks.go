// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package networks

import (
	"context"
	"errors"
	"fmt"

	"github.com/digineo/go-uci"
	log "github.com/sirupsen/logrus"
)

const vlanid = "1"

func Create(ctx context.Context, id string, vlanid int32) error {
	logger := log.WithFields(log.Fields{"network": id})
	logger.Info("creating network")
	sectionExists := uci.Set("network", id, "interface")
	if sectionExists == false {
		err := uci.AddSection("network", id, "interface")
		if err != nil {
			return err
		}
		_ = uci.Set("network", id, "ifname", fmt.Sprintf("eth0.%d", vlanid))
		_ = uci.Set("network", id, "proto", "static")
		_ = uci.Set("network", id, "ipaddr", "192.168.0.1")
		_ = uci.Set("network", id, "netmask", "255.255.255.0")
		err = uci.Commit()
		if err != nil {
			return err
		}
	} else {
		return errors.New("network already exists")
	}
	return nil
}

func Delete(ctx context.Context, id string, vlanid int32) error {
	logger := log.WithFields(log.Fields{"network": id})
	logger.Info("deleting network")
	sectionExists := uci.Set("network", id, "interface")
	if sectionExists == true {
		uci.DelSection("network", id)
		err := uci.Commit()
		if err != nil {
			return err
		}
	} else {
		return errors.New("network doesn't exist")
	}
	return nil
}

// config interface 'lan'
//    option type 'bridge'
//    option ifname 'eth0.106'
//    option proto 'static'
//    option ipaddr '192.168.1.1'
//    option netmask '255.255.255.0'
