// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package main

import (
	"net/rpc"
	"os"

	log "github.com/sirupsen/logrus"

	"git.callpipe.com/entanglement.garden/rhyzome-openwrt/common"
	"git.callpipe.com/entanglement.garden/rhyzome-openwrt/localrpc"
)

func main() {
	common.ConfigureLogging()

	client, err := rpc.DialHTTP("unix", localrpc.SocketPath)
	if err != nil {
		log.Fatal(err)
	}
	var reply int
	callArgs := localrpc.DHCPScriptCallback{
		Action:   localrpc.DHCPAction(os.Getenv("ACTION")),
		MACAddr:  os.Getenv("MACADDR"),
		IPAddr:   os.Getenv("IPADDR"),
		Hostname: os.Getenv("HOSTNAME"),
	}
	log.Debugf("dhcpscript called: %+v", callArgs)
	err = client.Call("DHCPScriptServer.Call", callArgs, &reply)
	if err != nil {
		log.Fatal(err)
	}
}
