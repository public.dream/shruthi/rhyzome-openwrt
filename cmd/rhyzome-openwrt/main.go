// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package main

import (
	"os"
	"os/signal"
	"syscall"

	log "github.com/sirupsen/logrus"

	"git.callpipe.com/entanglement.garden/rhyzome-openwrt/common"
	"git.callpipe.com/entanglement.garden/rhyzome-openwrt/config"
	"git.callpipe.com/entanglement.garden/rhyzome-openwrt/grpcclient"
	"git.callpipe.com/entanglement.garden/rhyzome-openwrt/localrpc"
	"git.callpipe.com/entanglement.garden/rhyzome-openwrt/metadata"
)

var signals = make(chan os.Signal, 1)

func main() {
	common.ConfigureLogging()
	config.Load()

	signal.Notify(signals, syscall.SIGINT, syscall.SIGTERM)

	if err := localrpc.ListenAndServe(); err != nil {
		log.Fatal("error starting local rpc server: ", err)
	}

	go metadata.ListenAndServe()

	log.Info("connecting")
	go grpcclient.Connect()

	for {
		signal := <-signals
		log.Debug("received signal", signal)
		switch signal {
		case syscall.SIGINT, syscall.SIGTERM:
			if err := localrpc.Shutdown(); err != nil {
				log.Error("error shutting down local rpc server: ", err)
			}
			if err := grpcclient.Shutdown(); err != nil {
				log.Error("error shutting down grpc client: ", err)
			}
			return
		}
	}

}
