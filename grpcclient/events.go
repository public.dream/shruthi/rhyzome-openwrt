// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package grpcclient

import (
	"context"

	log "github.com/sirupsen/logrus"

	"git.callpipe.com/entanglement.garden/rhyzome-openwrt/networks"
	"git.callpipe.com/entanglement.garden/rhyzome-protos/rhyzome_protos"
)

func processEvent(ctx context.Context, event *rhyzome_protos.OpenwrtEvent) {
	var err error
	switch {
	case event.CreateNetwork != nil:
		err = createNetwork(ctx, event)
	case event.DeleteNetwork != nil:
		err = deleteNetwork(ctx, event)
	}
	if err != nil {
		log.Error("error handling event: ", err)
		err = eventStreamClient.Send(&rhyzome_protos.OpenwrtEventUpdate{
			EventId: event.EventId,
			Type:    rhyzome_protos.OpenwrtEventUpdate_FAILED,
			Message: err.Error(),
		})
		if err != nil {
			log.Error("error submitting event update: ", err)
		}
	} else {
		err = eventStreamClient.Send(&rhyzome_protos.OpenwrtEventUpdate{
			EventId: event.EventId,
			Type:    rhyzome_protos.OpenwrtEventUpdate_SUCCESS,
		})
		if err != nil {
			log.Error("error submitting event update: ", err)
		}
	}
	backoff = 1
}

func createNetwork(ctx context.Context, event *rhyzome_protos.OpenwrtEvent) error {
	e := event.CreateNetwork
	err := networks.Create(ctx, e.ResourceId, e.VlanId)
	if err != nil {
		return err
	}
	return nil
}

func deleteNetwork(ctx context.Context, event *rhyzome_protos.OpenwrtEvent) error {
	e := event.DeleteNetwork
	err := networks.Delete(ctx, e.ResourceId, e.VlanId)
	if err != nil {
		return err
	}
	return nil
}
