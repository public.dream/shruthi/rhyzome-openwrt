// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package grpcclient

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"errors"
	"io"
	"io/ioutil"
	"time"

	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"

	"git.callpipe.com/entanglement.garden/rhyzome-openwrt/config"
	"git.callpipe.com/entanglement.garden/rhyzome-protos/rhyzome_protos"
)

const maxBackoff = 60 // max 60 second backoff

var (
	conn              *grpc.ClientConn
	openwrtClient     rhyzome_protos.RhyzomeOpenwrtClient
	eventStreamClient rhyzome_protos.RhyzomeOpenwrt_EventStreamClient
	shutdown          = false
	backoff           = 1
)

// Connect to the grpc server, retrying until Shutdown is called
func Connect() {
	for {
		if shutdown {
			log.Debug("shutdown = true, not re-connecting")
			return
		}

		apiserver := config.GetAPIServer()
		if apiserver == "" {
			time.Sleep(time.Second)
			continue
		}

		log.Debug("connecting to ", apiserver)

		delay := time.Duration(backoff) * time.Second
		log.Debug("connecting to grpc server in ", delay)
		time.Sleep(time.Duration(backoff) * time.Second)

		if err := connect(apiserver); err != nil {
			log.Error("error connecting to grpc server: ", err)
			increaseBackoff()
		}
	}
}

func connect(apiserver string) error {
	// parts := strings.Split(apiserver, ":")
	// host := parts[0]

	cert := config.GetCert()
	key := config.GetKey()

	log.Debug("loading x509 pair from ", cert, " and ", key)
	certificate, err := tls.LoadX509KeyPair(cert, key)
	if err != nil {
		log.Error("error loading x509 pair: ", err)
		return err
	}

	ca := config.GetCA()
	log.Debug("loading ca from ", ca)
	certPool := x509.NewCertPool()
	bs, err := ioutil.ReadFile(ca)
	if err != nil {
		log.Error("error reading CA file: ", err)
		return err
	}

	ok := certPool.AppendCertsFromPEM(bs)
	if !ok {
		err = errors.New("error appending root CA to pool")
		log.Error(err)
		return err
	}

	tlsConfig := credentials.NewTLS(&tls.Config{
		// ServerName:   host,
		Certificates: []tls.Certificate{certificate},
		RootCAs:      certPool,
	})

	ctx, cancel := context.WithTimeout(context.Background(), time.Minute)
	defer cancel()

	log.Info("establishing grpc connection to ", apiserver)
	conn, err = grpc.DialContext(ctx, apiserver, grpc.WithTransportCredentials(tlsConfig))
	if err != nil {
		log.Error("error dialing API server: ", err)
		return err
	}

	openwrtClient = rhyzome_protos.NewRhyzomeOpenwrtClient(conn)
	eventStreamClient, err = openwrtClient.EventStream(context.Background())
	if err != nil {
		log.Error("error getting event stream from openwrt client: ", err)
		return err
	}
	processEvents()

	return nil
}

// Shutdown the grpc client
func Shutdown() error {
	shutdown = true
	if conn == nil {
		return nil
	}
	log.Info("shutting down grpc client")
	return conn.Close()
}

func processEvents() {
	for {
		if shutdown {
			return
		}
		event, err := eventStreamClient.Recv()
		if err != nil {
			if err == io.EOF {
				log.Info("Got EOF from GRPC server")
			} else {
				log.Error("error receiving event from grpc stream: ", err)
			}
			increaseBackoff()
			return
		}

		go processEvent(eventStreamClient.Context(), event)
		backoff = 1
	}
}

func increaseBackoff() {
	backoff = backoff * 2
	if backoff > maxBackoff {
		backoff = maxBackoff
	}
}
