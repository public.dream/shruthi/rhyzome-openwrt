// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package common

import (
	"fmt"
	"log/syslog"
	"runtime"
	"strings"

	log "github.com/sirupsen/logrus"
	lSyslog "github.com/sirupsen/logrus/hooks/syslog"
)

// ConfigureLogging configures logrus for syslog output
func ConfigureLogging() {
	log.SetLevel(log.DebugLevel)
	log.SetReportCaller(true)
	log.SetFormatter(&log.TextFormatter{
		FullTimestamp:             true,
		EnvironmentOverrideColors: true,
		CallerPrettyfier:          logPrettifier,
	})

	hook, err := lSyslog.NewSyslogHook("", "", syslog.LOG_INFO, "rhyzome-openwrt")
	if err != nil {
		log.Error("error configuring syslog hook: ", err)
	}
	log.AddHook(hook)
}

func logPrettifier(f *runtime.Frame) (string, string) {
	filenameParts := strings.SplitN(f.File, "/rhyzome-openwrt/", 2)
	filename := f.File
	if len(filenameParts) > 1 {
		filename = fmt.Sprintf("%s:%d", filenameParts[1], f.Line)
	}
	s := strings.Split(f.Function, ".")
	funcname := fmt.Sprintf("%s()", s[len(s)-1])
	return funcname, filename
}
