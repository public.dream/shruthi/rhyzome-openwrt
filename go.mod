module git.callpipe.com/entanglement.garden/rhyzome-openwrt

go 1.15

require (
	git.callpipe.com/entanglement.garden/rhyzome-openapi v0.0.0-20220502192255-b41d9991a224
	git.callpipe.com/entanglement.garden/rhyzome-protos v0.0.0-20211027040613-7a88aa3a3c38
	github.com/digineo/go-uci v0.0.0-20210918132103-37c7b10c14fa
	github.com/golang-jwt/jwt/v4 v4.4.1
	github.com/gorilla/mux v1.8.0
	github.com/mostlygeek/arp v0.0.0-20170424181311-541a2129847a
	github.com/sirupsen/logrus v1.8.1
	google.golang.org/grpc v1.41.0
	gopkg.in/yaml.v2 v2.4.0
)
