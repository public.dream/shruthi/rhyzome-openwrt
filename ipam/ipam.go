// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package ipam

var (
	ipdb = make(map[string]string) // map[mac]ip
)

// Update the IPAM DB
func Update(mac string, ip string, hostname string) {
	ipdb[mac] = ip
}
