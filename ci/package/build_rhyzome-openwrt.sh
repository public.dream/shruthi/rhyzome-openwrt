#!/bin/bash
# Copyright 2021 Entanglement Garden Developers
# SPDX-License-Identifier: AGPL-3.0-only

set -exuo pipefail

pwd
ls -alh
mkdir -p ci/package/openwrt/rhyzome-openwrt/src
cp -arv ./* ci/package/openwrt/rhyzome-openwrt/src/ || true
cp -rv ci/package/openwrt/* /home/build/openwrt/package/.
cp -v ci/package/openwrt/feeds.conf /home/build/openwrt/.
cd /home//build/openwrt
./scripts/feeds update base packages entanglement
./scripts/feeds install -f rhyzome-openwrt
make defconfig
make package/rhyzome-openwrt/compile -j$(nproc) V=s
find . -iname "*.ipk"
mkdir -p $CI_PROJECT_DIR/bin
cp -rv /home/build/openwrt/bin/packages/x86_64/entanglement $CI_PROJECT_DIR/bin/
