# Rhyzome Openwrt

This is a Rhyzome component for networking using Openwrt.

It is expected to be used on an Openwrt VM running on a Hypervisor (or cluster) where you are deploying "instances" (VMs) with [Rhyzome Libvirt](https://git.callpipe.com/entanglement.garden/rhyzome-libvirt).

It facilitates the creation of new private networks and WireGuard endpoints.

## Install

You can build the package as a single binary using Go as follows:

`go build -o rhyzome-openwrt cmd/rhyzome-openwrt/main.go`


An IPK package is created using scripts in the `ci` directory. This is done automatically using Gitlab CI, but you can also run the `build_rhyzome-openwrt.sh` package locally if you setup an environment for using the Openwrt [ImageBuilder](https://openwrt.org/docs/guide-user/additional-software/imagebuilder).

## Setup

### Configuration

The program will look for a configuration file named `rhyzome-openwrt.json` in the directory where it is run, or in `/etc/rhyzome/`.

The config requires an `apiserver` and a `pki` section defined. The following example assumes you are running the `Rhyzome API` GRPC server on host `Iido2eewe` and default port `9090`. The PKI is explained in the next section.

```json
{
  "apiserver": "Iido2eewe:9090",
  "pki": {
      "ca": "/home/user/.step/certs/root_ca.crt",
      "cert": "/etc/step/certificates/step.crt",
      "key": "/etc/step/certificates/step.key"
  }
}
```

### PKI

Rhyzome uses Mutual TLS (mTLS) authentication between the components. You can read about how to setup a PKI and issue certificates using [Smallstep](https://github.com/smallstep) in our [PKI guide](https://git.callpipe.com/entanglement.garden/rhyzome-api/-/wikis/PKI-Guide).


## Usage

Once the configuration is in place, we can start the daemon by simply running:

```shell
./rhyzome-openwrt
```

If the configuration is correct the program will connect via GRPC to the Rhyzome API server and long poll for jobs.

You can schedule network creation jobs via Rhyzome API for testing by using curl like so:

```shell
curl -H 'Accept: application/json' -H "Authorization: Bearer ${TOKEN}" -X POST ${APISERVER}:8080/v0/network
```

A new network interface will be added to the Openwrt host, with a private subnet, and a WireGuard interface bridged to that network.
