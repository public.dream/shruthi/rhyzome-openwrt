// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package config

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"strings"

	log "github.com/sirupsen/logrus"
)

const (
	biosDataFile = "/sys/class/dmi/id/product_serial"
)

var (
	bootstrapMAC      string
	bootstrapUserData string
)

// autoconfig is a stupid name please suggest a better one
func autoconfig() error {
	biosdata := readBiosData()

	configURL := biosdata["s"]
	if configURL == "" {
		return errors.New("no detectable network config sources")
	}
	configURL = configURL + "router-bootstrap-data.json"

	log.Info("loading config from network: ", configURL)

	resp, err := http.Get(configURL)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	var config map[string]string
	err = json.NewDecoder(resp.Body).Decode(&config)
	if err != nil {
		return err
	}

	bootstrapMAC = config["bootstrap_mac"]
	bootstrapUserData = config["bootstrap_userdata"]

	return nil
}

func readBiosData() map[string]string {
	configbytes, err := ioutil.ReadFile(biosDataFile)
	if err != nil {
		return map[string]string{}
	}

	configstr := strings.TrimSpace(string(configbytes)) // remove trailing \n

	biosdata := map[string]string{}

	for _, s := range strings.Split(configstr, ";") {
		kv := strings.SplitN(s, "=", 2)
		if len(kv) != 2 {
			log.Warn("invalid product_serial component: ", s)
			continue
		}

		biosdata[kv[0]] = kv[1]
	}

	return biosdata
}
