// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package config

import (
	"errors"
	"fmt"
	"runtime"
	"strings"
	"syscall"
	"time"

	log "github.com/sirupsen/logrus"
)

type loadHook func() error

var loadHooks []loadHook

// Load attempts to read configuration from disk then falls back to alternative methods until successfully configured
func Load() {
	// attempt to load config from the configured autoconfig server
	for {
		if err := autoconfig(); err != nil {
			log.Error("error bootstrapping config: ", err)

			if errors.Is(err, syscall.ENETUNREACH) {
				log.Debug("retrying")
				time.Sleep(time.Second)
				continue
			}
		}
		break
	}

	for _, hook := range loadHooks {
		if err := hook(); err != nil {
			log.Fatal("error in config load hook: ", err)
		}
	}
}

func logPrettifier(f *runtime.Frame) (string, string) {
	filenameParts := strings.SplitN(f.File, "/rhyzome-openwrt/", 2)
	filename := f.File
	if len(filenameParts) > 1 {
		filename = fmt.Sprintf("%s:%d", filenameParts[1], f.Line)
	}
	s := strings.Split(f.Function, ".")
	funcname := fmt.Sprintf("%s()", s[len(s)-1])
	return funcname, filename
}

// OnLoad adds a new load hook
// we need to call code in other packages after Load() but we can't import those other packages into config,
// otherwise those packages wouldn't be able to import config. Thus this weirdness.
func OnLoad(hook loadHook) {
	loadHooks = append(loadHooks, hook)
}
