// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package config

import (
	uci "github.com/digineo/go-uci"

	log "github.com/sirupsen/logrus"
)

const (
	uciConfig  = "rhyzome"
	uciSection = "rhyzome"
	uciType    = "rhyzome"

	uciOptionAPIServer = "apiserver"
	uciOptionTLSCert   = "tls_cert"
	uciOptionTLSKey    = "tls_key"
	uciOptionTLSCA     = "tls_ca"
)

var (
	defaultTree = uci.NewTree(uci.DefaultTreePath)
)

// GetAPIServer returns the apiserver UCI config
func GetAPIServer() string {
	value, _ := defaultTree.GetLast(uciConfig, uciSection, uciOptionAPIServer)
	return value
}

// GetCert returns the tls_cert UCI config
func GetCert() string {
	value, _ := defaultTree.GetLast(uciConfig, uciSection, uciOptionTLSCert)
	return value
}

// GetKey returns the tls_key UCI config
func GetKey() string {
	value, _ := defaultTree.GetLast(uciConfig, uciSection, uciOptionTLSKey)
	return value
}

// GetCA returns the tls_ca UCI config
func GetCA() string {
	value, _ := defaultTree.GetLast(uciConfig, uciSection, uciOptionTLSCA)
	return value
}

// GetBootstrapMAC returns the bootstrap_mac UCI config
func GetBootstrapMAC() string {
	return bootstrapMAC
}

// GetBootstrapUserData returns the bootstrap_userdata
func GetBootstrapUserData() []byte {
	return []byte(bootstrapUserData)
}

// ConfigurePKI Stores the PKI config
func ConfigurePKI(tlsCert, tlsKey, tlsCA string) error {
	if err := ensureSection(); err != nil {
		log.Error("error ensuring uci section exists")
		return err
	}
	defaultTree.SetType(uciConfig, uciSection, uciOptionTLSCert, uci.TypeOption, tlsCert)
	defaultTree.SetType(uciConfig, uciSection, uciOptionTLSKey, uci.TypeOption, tlsKey)
	defaultTree.SetType(uciConfig, uciSection, uciOptionTLSCA, uci.TypeOption, tlsCA)
	return defaultTree.Commit()
}

// SetAPIServer updates the configured apiserver address
func SetAPIServer(apiserver string) error {
	if err := ensureSection(); err != nil {
		log.Error("error ensuring uci section exists")
		return err
	}
	defaultTree.SetType(uciConfig, uciSection, uciOptionAPIServer, uci.TypeOption, apiserver)
	return defaultTree.Commit()
}

func ensureSection() error {
	sections, _ := defaultTree.GetSections(uciConfig, uciSection)
	for _, s := range sections {
		if s == uciSection {
			return nil
		}
	}

	log.Debug("adding uci section ", uciSection)
	if err := defaultTree.AddSection(uciConfig, uciSection, uciType); err != nil {
		return err
	}

	return nil
}
