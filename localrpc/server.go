// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package localrpc

import (
	"context"
	"net"
	"net/http"
	"net/rpc"
	"os"
	"path"
)

const (
	SocketPath = "/var/run/rhyzome/localrpc.sock"
)

var server = &http.Server{}

// ListenAndServe creates a listener on the unix socket and starts the server in a goroutine
func ListenAndServe() error {
	rpc.HandleHTTP()

	if err := os.Mkdir(path.Dir(SocketPath), 0750); err != nil && !os.IsExist(err) {
		return err
	}

	listener, err := net.Listen("unix", SocketPath)
	if err != nil {
		return err
	}
	go server.Serve(listener)
	return nil
}

// Shutdown shuts down the server
func Shutdown() error {
	if err := server.Shutdown(context.Background()); err != nil {
		return err
	}

	if err := os.Remove(SocketPath); err != nil && err != os.ErrNotExist {
		return err
	}

	return nil
}
