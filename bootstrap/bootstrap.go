// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package bootstrap

import (
	"fmt"
	"net/url"
	"os"
	"os/exec"
	"time"

	log "github.com/sirupsen/logrus"

	"git.callpipe.com/entanglement.garden/rhyzome-openwrt/config"
)

const (
	steppath = "/etc/step"
	certPath = "/etc/rhyzome/host.crt"
	keyPath  = "/etc/rhyzome/host.key"
	caPath   = "/etc/rhyzome/ca.pem"
)

var retryTime = time.Second

// Begin does the initial certificate issuances and hooks up the config
func Begin(caFingerprint, token, ip string) {
	log.Info("beginning boostrapping for ", ip)

	ConfigureUCI(ip)

	provisioningInfo, err := getProvisioningInfo(url.URL{Scheme: "https", Host: ip}, caFingerprint)
	if err != nil {
		log.Warn("error getting bootstrap provisioning info: ", err)
		go retry(caFingerprint, token, ip)
		return
	}
	log.Debug("Got bootstrap provisioning info")

	if _, err := os.Stat("/root/.step/certs/root_ca.crt"); os.IsNotExist(err) {
		if err := runStepCommand("ca", "bootstrap", "--ca-url", provisioningInfo.Endpoints.Step, "--fingerprint", caFingerprint); err != nil {
			log.Error("error initializing step locally: ", err)
			go retry(caFingerprint, token, ip)
			return
		}
		log.Debug("configured step locally")
	}

	if _, err := os.Stat(certPath); os.IsNotExist(err) {
		subject, err := getSubject(token)
		if err != nil {
			log.Error("error parsing token: ", err)
			go retry(caFingerprint, token, ip)
			return
		}
		if err := runStepCommand("ca", "certificate", "--token", token, subject, certPath, keyPath); err != nil {
			log.Error("error getting certificate: ", err)
			go retry(caFingerprint, token, ip)
			return
		}
		log.Info("local certificates configured in ", certPath)
	}

	if err := config.ConfigurePKI(certPath, keyPath, caPath); err != nil {
		log.Error("error storing config for PKI: ", err)
		go retry(caFingerprint, token, ip)
		return
	}

	if err := config.SetAPIServer(fmt.Sprintf("%s:9090", ip)); err != nil {
		log.Error("error storing config for API server: ", err)
		go retry(caFingerprint, token, ip)
		return
	}
}

func retry(caFingerprint, token, ip string) {
	log.Debug("retrying bootstrapping in ", retryTime)
	time.Sleep(retryTime)
	if retryTime < time.Minute {
		retryTime = retryTime * 2
	}
	Begin(caFingerprint, token, ip)
}

func runStepCommand(args ...string) error {
	log.Debug("executing step: ", args)

	infoLogger := log.StandardLogger().WriterLevel(log.InfoLevel)
	defer infoLogger.Close()
	warnLogger := log.StandardLogger().WriterLevel(log.WarnLevel)
	defer warnLogger.Close()

	bootstrapCmd := exec.Command("step", args...)
	bootstrapCmd.Env = append(bootstrapCmd.Env, fmt.Sprintf("STEPPATH=%s", steppath))
	bootstrapCmd.Stdout = infoLogger
	bootstrapCmd.Stderr = warnLogger
	if err := bootstrapCmd.Run(); err != nil {
		return err
	}
	return nil
}
