// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package bootstrap

import (
	"github.com/golang-jwt/jwt/v4"
)

var jwtParser = jwt.Parser{}

func getSubject(tokenString string) (string, error) {
	var claims jwt.MapClaims
	_, _, err := jwtParser.ParseUnverified(tokenString, &claims)
	if err != nil {
		return "", err
	}

	return claims["sub"].(string), nil
}
