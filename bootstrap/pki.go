// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package bootstrap

import (
	"crypto/sha256"
	"crypto/tls"
	"crypto/x509"
	"encoding/hex"
	"encoding/json"
	"encoding/pem"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"strings"

	log "github.com/sirupsen/logrus"

	rhyzome "git.callpipe.com/entanglement.garden/rhyzome-openapi"
)

func getProvisioningInfo(base url.URL, caFingerprint string) (p rhyzome.ProvisioningInfo, err error) {
	client, ca, err := getClient(base, caFingerprint)
	if err != nil {
		return p, err
	}

	err = os.MkdirAll("/etc/rhyzome", 755)
	if err != nil {
		return p, err
	}

	f, err := os.Create(caPath)
	if err != nil {
		return p, err
	}
	defer f.Close()

	f.Write(ca)

	base.Path = base.Path + "/.well-known/rhyzome/provisioning.json"
	log.Debug("requesting ", base.String())
	resp, err := client.Get(base.String())
	if err != nil {
		return p, err
	}
	defer resp.Body.Close()

	err = json.NewDecoder(resp.Body).Decode(&p)
	if err != nil {
		return p, err
	}

	return p, nil
}

// getClient downloads and validates the CA root certificate, then returns an http client that trusts it
func getClient(u url.URL, caFingerprint string) (*http.Client, []byte, error) {
	u.Path = u.Path + "/.well-known/rhyzome/ca.pem"

	// insecureClient is an https client that doesn't validate certificates
	insecureClient := &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: true,
			},
		},
	}

	log.Debug("requesting ", u.String())
	resp, err := insecureClient.Get(u.String())
	if err != nil {
		return nil, nil, err
	}
	defer resp.Body.Close()

	caPem, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, nil, err
	}

	block, rest := pem.Decode(caPem)
	if block == nil {
		log.Debug("rest of ca cert: ", string(rest))
		return nil, nil, errors.New("error decoding certificate")
	}

	cert, err := x509.ParseCertificate(block.Bytes)
	if err != nil {
		return nil, nil, err
	}

	sum := sha256.Sum256(cert.Raw)
	actualFingerprint := strings.ToLower(hex.EncodeToString(sum[:]))
	if !strings.EqualFold(caFingerprint, actualFingerprint) {
		return nil, nil, fmt.Errorf("ca did not match expected fingerprint (expected %s got %s)", caFingerprint, actualFingerprint)
	}

	pool := x509.NewCertPool()
	pool.AppendCertsFromPEM(caPem)

	return &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				RootCAs: pool,
			},
		},
	}, caPem, nil
}
