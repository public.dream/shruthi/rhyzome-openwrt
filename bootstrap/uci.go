package bootstrap

import (
	"os/exec"

	uci "github.com/digineo/go-uci"
	log "github.com/sirupsen/logrus"
)

const apiserverPlaceholder = "apiserver"

var (
	firewallUCI = map[string][]map[string]string{
		"rule": []map[string]string{
			map[string]string{
				"name":      "ssh",
				"src":       "wan",
				"proto":     "tcp",
				"dest_port": "22",
				"target":    "ACCEPT",
				"family":    "ipv4",
			},
		},
		"redirect": []map[string]string{
			map[string]string{
				"name":      "apiserver_ssh",
				"src":       "wan",
				"src_dport": "2222",
				"dest":      "lan",
				"dest_ip":   apiserverPlaceholder,
				"dest_port": "22",
				"proto":     "tcp",
				"target":    "DNAT",
			},
			map[string]string{
				"name":      "apiserver_http",
				"src":       "wan",
				"src_dport": "80",
				"dest":      "lan",
				"dest_ip":   apiserverPlaceholder,
				"dest_port": "80",
				"proto":     "tcp",
				"target":    "DNAT",
			},
			map[string]string{
				"name":      "apiserver_https",
				"src":       "wan",
				"src_dport": "443",
				"dest":      "lan",
				"dest_ip":   apiserverPlaceholder,
				"dest_port": "443",
				"proto":     "tcp",
				"target":    "DNAT",
			},
		},
	}
	defaultTree = uci.NewTree(uci.DefaultTreePath)
)

// ConfigureUCI updates the firewall to allow access to the apiserver
func ConfigureUCI(apiserver string) error {
	log.Debug("updating firewall config in uci")
	needsCommit := false
	for typ, sections := range firewallUCI {
		existing, ok := defaultTree.GetSections("firewall", typ)
		if ok {
			log.Debug("existing ", typ, " sections: ", existing)
		}

		for _, section := range sections {
			sectionName := section["name"]

			add := true
			for _, e := range existing {
				if sectionName == e {
					add = false
					break
				}
			}

			if add {
				log.Info("adding section ", sectionName)
				if err := defaultTree.AddSection("firewall", sectionName, typ); err != nil {
					log.Error("error adding section")
					return err
				}
			}

			for k, v := range section {
				value := v
				if v == apiserverPlaceholder {
					value = apiserver
				}

				current, _ := defaultTree.GetLast("firewall", sectionName, k)

				if current != value {
					if defaultTree.SetType("firewall", sectionName, k, uci.TypeOption, value) {
						log.Info("set uci option firewall.", sectionName, ".", k, "=", value)
						needsCommit = true
					}
				}
			}
		}
	}

	if needsCommit {
		log.Info("committing")
		if err := defaultTree.Commit(); err != nil {
			log.Error("error committing uci changes")
			return err
		}

		if err := exec.Command("/etc/init.d/firewall", "reload").Run(); err != nil {
			log.Error("error reloading firewall rules")
			return err
		}
	}
	return nil
}
